package com.example.sjayasinghe.colombotvprograms;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class PostFeed extends AppCompatActivity {

    private ExpandableHeightListView listview;
    private ArrayList<Bean> Bean;
    private ListBaseAdapter baseAdapter;

    String progName="";
    Integer programID;
    Connectivity IP = new Connectivity();

    /*private int[] IMAGE1 = {R.drawable.newsname1, R.drawable.newsname1, R.drawable.newsname1,R.drawable.newsname1};
    private int[] IMAGE2 = {R.drawable.img1, R.drawable.img1, R.drawable.img1,R.drawable.img1};
    private int[] IMAGE3 = {R.drawable.more, R.drawable.more, R.drawable.more, R.drawable.more};
    private String[] NEWSNAME = {"Fox News .", "Fox News .", "Fox News .","ColomboTV Eagle"};
    private String[] TITLE = {"1 day ago", "1 day ago", "1 day ago","2 days ago"};
    private String[] NEWS = {"Trump’s Plan for AmericanMade iPhonew Wold Be Disastrous. Trump’s Plan for AmericanMade iPhonew Wold Be Disastrous",
            "Trump’s Plan for AmericanMade iPhonew Wold Be Disastrous. Trump’s Plan for AmericanMade iPhonew Wold Be Disastrous",
            "Trump’s Plan for AmericanMade iPhonew Wold Be Disastrous. Trump’s Plan for AmericanMade iPhonew Wold Be Disastrous","These ambulances can be contacted by using the toll free numbers such as 119 or 118.Minister Harsha Dissanayake expects feedback from users.."};
    private String[] NEWSSUB = {"Why even a President Trump couldn’t make Apple manufacture iPhone in the state.","Why even a President Trump couldn’t make Apple manufacture iPhone in the state.",
            "Why even a President Trump couldn’t make Apple manufacture iPhone in the state.","These ambulances can be contacted by using the toll free numbers such as 119 or 118.Minister Harsha Dissanayake expects feedback from users.."};
    private String[] INTREST = {"You've s/hown interest in iPhone","You've shown interest in iPhone","You've shown interest in iPhone","You have shown interest in government"};
*/
    /**
     * Declare for Card List
     */
    public ArrayList<Integer> IMAGE1 = new ArrayList<>();
    public ArrayList<Integer> IMAGE2 = new ArrayList<>();
    public ArrayList<Integer> IMAGE3 = new ArrayList<>();
    public ArrayList<String> NEWSNAME = new ArrayList<>();
    public ArrayList<String> DATE = new ArrayList<>();
    public ArrayList<String> NEWS = new ArrayList<>();
    public ArrayList<String> NEWSSUB = new ArrayList<>();
    public ArrayList<String> INTREST = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_feed);

        Intent intent = getIntent();
        progName = intent.getStringExtra("programName");
        programID = Integer.parseInt(intent.getStringExtra("programID"));
        Initialize();
    }

    public void Initialize(){
        ImageView img = (ImageView)findViewById(R.id.coverClick);
        img.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://www.youtube.com/watch?v=sX0fgfepf5k"));
                startActivity(intent);
            }
        });

        listview = (ExpandableHeightListView)findViewById(R.id.listview);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),"You have clicked on this item card!",Toast.LENGTH_SHORT).show();
            }
        });
        new jsonClass().execute("http://"+IP.getMyIP()+"/Android/databaseOne.php?programID="+programID);
    }

    /**
     * AsyncTask
     */
    public class jsonClass extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... parms){
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try{
                URL url = new URL(parms[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader((stream)));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while((line = reader.readLine()) != null ){
                    buffer.append(line);
                }

                return buffer.toString(); // return String of HTTP request

            }catch (MalformedURLException e){
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            } finally {
                if(connection != null){
                    connection.disconnect();
                }
                try{
                    if(reader != null){
                        reader.close();
                    }
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            //System.out.println("RESUL = "+result);
            Log.d("DDD = ",R.drawable.newsname1+"");
            Log.d("Result = ",result);
            if(!result.equals("[]")){
                try{
                    JSONObject data = new JSONObject(result);
                    for(int i=0;i<data.length();i++){
                        IMAGE1.add(R.drawable.newsname1);
                        IMAGE2.add(R.drawable.img1);
                        IMAGE3.add(R.drawable.more);
                        NEWSNAME.add(progName);
                        DATE.add(data.getJSONObject("data_"+i).getString("progSegementDate").toString());
                        NEWS.add(data.getJSONObject("data_"+i).getString("progSegmentData").toString());
                        NEWSSUB.add(data.getJSONObject("data_"+i).getString("progSegmentTitle").toString());
                        INTREST.add("You've shown interest in iPhone");
                        //String programName = data.getJSONObject("");
                        Log.d("DATA = ",data.getJSONObject("data_"+i).toString());
                        //programListData.add(data.getJSONObject("program_"+i).getString("progName").toString());
                    }

                    Bean = new ArrayList<Bean>();

                    for (int i= 0; i< NEWSNAME.size(); i++){

                        Bean bean = new Bean(IMAGE1.get(i), IMAGE2.get(i), IMAGE3.get(i), NEWSNAME.get(i), DATE.get(i), NEWS.get(i), NEWSSUB.get(i), INTREST.get(i));
                        Bean.add(bean);

                    }
                    baseAdapter = new ListBaseAdapter(PostFeed.this, Bean) {
                    };

                    listview.setAdapter(baseAdapter);
                }catch(JSONException e){
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(getApplication(),"No Programs Found",Toast.LENGTH_LONG).show();
            }
        }
    }

}

package com.example.sjayasinghe.colombotvprograms;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class userLogin extends AppCompatActivity {

    private EditText userName;
    private EditText password;
    Connectivity connec = new Connectivity();
    Login loginData = new Login();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);

                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        userName = (EditText)findViewById(R.id.usernameInput);
        password = (EditText) findViewById((R.id.password));

        final Button login = (Button)findViewById(R.id.login_button);
        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String user =String.valueOf(userName.getText());
                String pass =String.valueOf(password.getText());

                //Validation

                if(user.equals("admin")&&(pass.equals("admin"))){
                    Context context = getApplicationContext();
                    CharSequence text = "Login Successfull!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                    Intent home = new Intent(userLogin.this,programList.class);

                    //pass login data to the Login class
                    loginData.setName("Sandunil");
                    startActivity(home);

                }else
                {
                    Context context = getApplicationContext();
                    CharSequence text = "Login Failed!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
        });


    }

}

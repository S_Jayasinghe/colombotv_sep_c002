package com.example.sjayasinghe.colombotvprograms;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telecom.RemoteConference;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class chatMainActivity extends AppCompatActivity implements View.OnClickListener,MessageDataSource.MessagesCallbacks {

    public static final String USER_EXTRA = "USER";

    public static final String TAG = "ChatActivity";

    private ArrayList<Message> mMessages;
    private MessagesAdapter mAdapter;
    private String mRecipient;
    private ListView mListView;
    private Date mLastMessageDate = new Date();
    private String mConvoId;
    private MessageDataSource.MessagesListener mListener;
    List<Message> Message_List;
    private MessageDataSource datasource;
    public static SharedPreferences noteprefs;
    HashMap<String,String> MapListMessages = new HashMap<String,String>();
    private ImageView translateButton;
    private ImageView speechAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat_main);

        Intent intent = getIntent();

        Firebase.setAndroidContext(this);

        /**
         * check for program Name present or user name
         */




        if(intent.getStringExtra("programName") != null) {
            // program list chat
            Log.d("Res = ", intent.getStringExtra("programName"));

            //recipient name on title bar
            mRecipient = intent.getStringExtra("programName");
            mListView = (ListView) findViewById(R.id.messages_list);
            mMessages = new ArrayList<>();
            mAdapter = new MessagesAdapter(mMessages);
            mListView.setAdapter(mAdapter);

            setTitle(mRecipient);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }

            Button sendMessage = (Button) findViewById(R.id.send_message);
            sendMessage.setOnClickListener(this);

            String[] ids = {"Ajay", "-", "Kumar"};
            Arrays.sort(ids);
            //mConvoId = ids[0]+ids[1]+ids[2];
            if (mRecipient.contains(" ")) {
                mConvoId = mRecipient.replace(' ', '_');
            } else {
                mConvoId = mRecipient;
            }

            mListener = MessageDataSource.addMessagesListener(mConvoId, this);


            translateButton = (ImageView) findViewById(R.id.translateButton);
            translateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                /*Intent translatorIntent = new Intent(getApplication(),translator.class);
                startActivity(translatorIntent);*/


                    Toast.makeText(getApplication(),"Clicked",Toast.LENGTH_LONG).show();
                }
            });

           /* speechAction = (ImageView)findViewById(R.id.speech);
            speechAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(getApplication(),"Clicked",Toast.LENGTH_LONG).show();
                }
            });*/
        } else if(intent.getStringExtra("userName") != null){
            // user chat
            Log.d("User Name = ",intent.getStringExtra("userName"));

            //recipient name on title bar
            mRecipient = intent.getStringExtra("userName");
            mListView = (ListView) findViewById(R.id.messages_list);
            mMessages = new ArrayList<>();
            mAdapter = new MessagesAdapter(mMessages);
            mListView.setAdapter(mAdapter);

            setTitle(mRecipient);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }

            Button sendMessage = (Button) findViewById(R.id.send_message);
            sendMessage.setOnClickListener(this);


            if (mRecipient.contains(" ")) {
                mConvoId = mRecipient.replace(' ', '_');
            } else {
                mConvoId = mRecipient;
            }
            mListener = MessageDataSource.addMessagesListener(mConvoId, this);


            translateButton = (ImageView) findViewById(R.id.translateButton);
            translateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   /* Intent translatorIntent = new Intent(getApplication(),translator.class);
                    startActivity(translatorIntent);*/


                    Toast.makeText(getApplication(),"Clicked",Toast.LENGTH_LONG).show();


                }
            });

            speechAction = (ImageView)findViewById(R.id.speech);
            speechAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(getApplication(),"Clicked",Toast.LENGTH_LONG).show();
                }
            });

        }
    }

    @Override
    public void onMessageAdded(Message message) {
        mMessages.add(message);
        mAdapter.notifyDataSetChanged();
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        MessageDataSource.stop(mListener);

    }


    @Override
    public void onClick(View v) {

        //Take the typed message content from the edit text box.
        //Use this to provide a string from the voice data.

        EditText newMessageView = (EditText)findViewById(R.id.new_message);
        String newMessage = newMessageView.getText().toString();
        newMessageView.setText("");
        Message msg = new Message();
        msg.setmDate(new Date());
        msg.setmText(newMessage);
        msg.setmSender("");

        MessageDataSource.saveMessage(msg, mConvoId, new  Login().getName());



    }

    public void FireSpeech(View view) {
        //Handles the speech to text
        Toast.makeText(getApplication(), "Clicked Me", Toast.LENGTH_LONG).show();

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        //Indicating the way to recognise the speech.API Stuff
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speech_input_phrase));

        try {
            startActivityForResult(intent, 100);

        } catch (ActivityNotFoundException errorCode) {
           // Toast.makeText(getApplication(), "Speech To Text Error Occured and Handled Successfully", Toast.LENGTH_LONG).show();
        Toast.makeText(this,getString(R.string.stt_not_supported_message),Toast.LENGTH_LONG).show();

        }

    }
        protected void onActivityResult(int requestNum,int resultNum,Intent speechData){

            if((requestNum==100)&&(speechData != null)&&(resultNum == RESULT_OK)){
                ArrayList<String> dataSpeech = speechData.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                //Set the grabbed data to the edit text box
                EditText spokenText = (EditText)findViewById(R.id.new_message);
                spokenText.setText(dataSpeech.get(0));
                Toast.makeText(getApplication(), "Tada", Toast.LENGTH_LONG).show();
            }

        }




    private class MessagesAdapter extends ArrayAdapter<Message> {
        MessagesAdapter(ArrayList<Message> messages){
            super(chatMainActivity.this, R.layout.message_item, R.id.message, messages);
        }


        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = super.getView(position, convertView, parent);
            Message message = getItem(position);

            TextView nameView = (TextView)convertView.findViewById(R.id.message);
            nameView.setText(message.getmText());

            TextView userName = (TextView) convertView.findViewById(R.id.userName);
            userName.setText(message.getmSender());
            // To hide the message sender's name
            //userName.setText("");

            RelativeLayout messageLayout = (RelativeLayout) convertView.findViewById(R.id.messageLayout);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)messageLayout.getLayoutParams();



            int sdk = Build.VERSION.SDK_INT;
            Login user = new Login();
            if (message.getmSender().equals(user.getName().replaceAll(" ",""))){
              
                messageLayout.setBackground(getDrawable(R.drawable.bubble_right_green));
                
                layoutParams.gravity = Gravity.RIGHT;
            }else{
               
                messageLayout.setBackground(getDrawable(R.drawable.bubble_left_gray));
                
                layoutParams.gravity = Gravity.LEFT;
            }

            
            messageLayout.setLayoutParams(layoutParams);

            return convertView;
        }
    }

}

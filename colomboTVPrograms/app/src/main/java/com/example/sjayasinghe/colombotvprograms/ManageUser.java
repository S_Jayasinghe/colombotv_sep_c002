package com.example.sjayasinghe.colombotvprograms;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by S.Jayasinghe on 10/17/2016.
 */
public class ManageUser {
    private Connectivity IP = new Connectivity();
    private String username;
    private String email;

    public ManageUser(){}

    /**
     * parameterized constructor for set user details
      * @param param_1
     * @param param_2
     */
    public ManageUser(String param_1,String param_2){
        this.username = param_1;
        this.email = param_2;
    }

    /**
     * save user details into db
     * @return
     */
    public boolean save(){

        new jsonClass().execute("http://"+IP.getMyIP()+"/Android/insertUser.php?username="+this.username+"&email="+this.email);

        return true;
    }


    /**
     * AsyncTask
     */
    public class jsonClass extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... parms){
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try{
                URL url = new URL(parms[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader((stream)));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while((line = reader.readLine()) != null ){
                    buffer.append(line);
                }

                return buffer.toString(); // return String of HTTP request

            }catch (MalformedURLException e){
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            } finally {
                if(connection != null){
                    connection.disconnect();
                }
                try{
                    if(reader != null){
                        reader.close();
                    }
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            Log.d("User Insert Status = ",result);
        }
    }
}

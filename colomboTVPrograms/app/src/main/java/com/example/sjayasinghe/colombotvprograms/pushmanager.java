package com.example.sjayasinghe.colombotvprograms;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by S.Jayasinghe on 9/30/2016.
 */
public class pushmanager {
    private static pushmanager singleInstance;
    private static Context contectvar;
    private RequestQueue requestQueue;

    private pushmanager(Context context){

        contectvar = context;
        requestQueue = getRequestQueue();
    }

    private RequestQueue getRequestQueue(){
        if(requestQueue==null){
            requestQueue= Volley.newRequestQueue(contectvar.getApplicationContext());

        }
        return requestQueue;
    }

    public static synchronized pushmanager getInstance(Context context){
        if(singleInstance==null){
            singleInstance=new pushmanager(context);
        }
        return singleInstance;
    }

    public<T> void addRequest(Request<T> request)
    {
        getRequestQueue().add(request);
    }


}

package com.example.sjayasinghe.colombotvprograms;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class pushmain extends AppCompatActivity {
    Button button;
    String appServer = "http://10.0.2.2/fcmtest/fcm_insert.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pushmain);
        button = (Button) findViewById(R.id.pusher);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedData = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
                //Get the Registration token from the Shared Preference.
                final String token = sharedData.getString(getString(R.string.FCM_TOKEN), "");
                StringRequest stringRequest = new StringRequest(Request.Method.POST, appServer,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }

                }) {
                    //Send data to server,therefore Override getparams
                    protected Map<String, String> getParams() throws AuthFailureError{
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("fcm_token",token);

                        return params;
                    }

                };
                pushmanager.getInstance(pushmain.this).addRequest(stringRequest);
            }
        });


        }
    }


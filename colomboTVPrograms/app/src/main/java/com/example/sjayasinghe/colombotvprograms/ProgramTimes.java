package com.example.sjayasinghe.colombotvprograms;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ProgramTimes extends AppCompatActivity{

    private ArrayList<ProgramData> programList = new ArrayList<>();
    Spinner daySpinner;
    ListView programSchedule;

    Intent localIntent;
    private Connectivity IP = new Connectivity();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_times);

        // initialize content
        initialize();
    }

    private void initialize(){
        daySpinner = (Spinner) findViewById(R.id.daySpinner);
        programSchedule = (ListView) findViewById(R.id.programSchedule);
        localIntent = getIntent();

        setTitle(localIntent.getStringExtra("day"));
        loadSpinner(localIntent.getStringExtra("day"));

        new jsonClass().execute("http://" + IP.getMyIP() + "/Android/schedule.php?programDay="+localIntent.getStringExtra("day"));
    }

    /**
     * loads day spinner
     * @param day
     */
    private void loadSpinner(String day){
        List<String> list = new ArrayList<>();
        list.add("Monday");
        list.add("Tu");
        list.add("Wednesday");
        list.add("TH");
        list.add("FR");
        list.add("Saturday");
        list.add("Sunday");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daySpinner.setAdapter(dataAdapter);

        daySpinner.setSelection(dataAdapter.getPosition(day), false);

        daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // On selecting a spinner item
                String selectedDay = daySpinner.getSelectedItem().toString();

                // start new Intent to selected day
                Intent newIntent = new Intent(getApplication(),ProgramTimes.class);
                newIntent.putExtra("day",selectedDay);
                startActivity(newIntent);

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    /**
     * AsyncTask
     */
    public class jsonClass extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... parms) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(parms[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader((stream)));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                return buffer.toString(); // return String of HTTP request

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            String newThreadName = "";

            Log.d("Result = ",result);

            if (!result.equals("[]")) {
                try {
                    JSONObject data = new JSONObject(result);
                    for (int i = 0; i < data.length(); i++) {
                        Log.d("DATA = ", data.getJSONObject("program_" + i).toString());

                        ProgramData programData = new ProgramData();

                        programData.setProgramID(data.getJSONObject("program_" + i).getInt("programID"));
                        programData.setProgramName(data.getJSONObject("program_" + i).getString("programName").toString());
                        programData.setProgramDes(data.getJSONObject("program_" + i).getString("programDescrip").toString());
                        programData.setProgramDay(data.getJSONObject("program_" + i).getString("programDay").toString());
                        programData.setProgramStart(data.getJSONObject("program_" + i).getString("programStart").toString());
                        programData.setProgramEnd(data.getJSONObject("program_" + i).getString("programEnd").toString());

                        programList.add(programData);
                    }

                    programSchedule.setAdapter(new MyCustomBaseAdapter(getApplicationContext(), programList));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplication(), "No programs Found", Toast.LENGTH_LONG).show();
            }
        }
    }


    /*
     *  Custom Array Adapter
     */
    public class MyCustomBaseAdapter extends BaseAdapter {
        private ArrayList<ProgramData> searchArrayList;

        private LayoutInflater mInflater;

        public MyCustomBaseAdapter(Context context, ArrayList<ProgramData> results) {
            searchArrayList = results;
            mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return searchArrayList.size();
        }

        public Object getItem(int position) {
            return searchArrayList.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.schedule_row, null);
                holder = new ViewHolder();

                holder.programTitle = (TextView) convertView.findViewById(R.id.programTitle);
                holder.programTime = (TextView) convertView.findViewById(R.id.programTime);
                holder.schedule_row = (LinearLayout) convertView.findViewById(R.id.schedule_row);
                holder.btnReminder = (ImageView) convertView.findViewById(R.id.btnReminder);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final ProgramData temp_data = searchArrayList.get(position);

            holder.programTitle.setText(temp_data.getProgramName());
            holder.programTime.setText(temp_data.getProgramStart()+" - "+temp_data.getProgramEnd());

            holder.btnReminder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("Click = ",temp_data.getProgramID()+"");
                }
            });

            return convertView;
        }

        public class ViewHolder {
            TextView programTitle,programTime;
            ImageView btnReminder;
            LinearLayout schedule_row;
        }
    }
    /*
     *  Custom Array Adapter
     */


    /**
     * custom class for Program Data
     */
    public class ProgramData{
        private int programID;
        private String programName;
        private String programDes;
        private String programDay;
        private String programStart;
        private String programEnd;

        public int getProgramID() {
            return programID;
        }

        public void setProgramID(int programID) {
            this.programID = programID;
        }

        public String getProgramName() {
            return programName;
        }

        public void setProgramName(String programName) {
            this.programName = programName;
        }

        public String getProgramDes() {
            return programDes;
        }

        public void setProgramDes(String programDes) {
            this.programDes = programDes;
        }

        public String getProgramDay() {
            return programDay;
        }

        public void setProgramDay(String programDay) {
            this.programDay = programDay;
        }

        public String getProgramStart() {
            return programStart;
        }

        public void setProgramStart(String programStart) {
            this.programStart = programStart;
        }

        public String getProgramEnd() {
            return programEnd;
        }

        public void setProgramEnd(String programEnd) {
            this.programEnd = programEnd;
        }
    }
}

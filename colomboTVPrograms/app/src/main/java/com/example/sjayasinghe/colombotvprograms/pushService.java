package com.example.sjayasinghe.colombotvprograms;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by S.Jayasinghe on 9/30/2016.
 */
public class pushService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh(){
        String latestToken = FirebaseInstanceId.getInstance().getToken();
        SharedPreferences sharedData = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor =sharedData.edit();
        editor.putString(getString(R.string.FCM_TOKEN),latestToken);

    }
}

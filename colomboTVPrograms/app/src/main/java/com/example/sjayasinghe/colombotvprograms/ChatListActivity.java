package com.example.sjayasinghe.colombotvprograms;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class ChatListActivity extends AppCompatActivity {
    // constant variables
    private static String GET_USER_LIST = "getUserList";
    private static String GET_CHAT_THREAD_LIST = "getChatThreadList";
    private static String INSERT_CHAT_THREAD = "insertChatThread";

    ListView chatUsersList;
    private Connectivity IP = new Connectivity();
    private ArrayList<String> chatUserListData = new ArrayList<>();
    private ArrayList<Integer> chatUserListDataID = new ArrayList<>();

    private static String asyncTaskType = ""; // stores current async task type

    private static String selectedUserName = ""; // stores selected user name
    private static Integer selectedUserID = 0; // stores selected user ID

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);

        initialize();
    }

    private void initialize() {
        chatUsersList = (ListView) findViewById(R.id.chatUsersList);

        // gets user list from remote DB
        ChatListActivity.asyncTaskType = ChatListActivity.GET_USER_LIST; // set current state
        new jsonClass().execute("http://" + IP.getMyIP() + "/Android/userList.php");

        chatUsersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), chatUserListData.get(position).toString(), Toast.LENGTH_SHORT).show();

                ChatListActivity.selectedUserName = chatUserListData.get(position).toString(); // parse selected user name
                ChatListActivity.selectedUserID = chatUserListDataID.get(position); // parse selected user ID

                ChatListActivity.asyncTaskType = ChatListActivity.GET_CHAT_THREAD_LIST; // set current state
                new jsonClass().execute("http://" + IP.getMyIP() + "/Android/chatThreadNameList.php?getList=OK");

//                Intent intent = new Intent(getApplication(), chatMainActivity.class);
//                intent.putExtra("userName", chatUserListData.get(position).toString());
//                intent.putExtra("userID", chatUserListDataID.get(position).toString());
//
//                startActivity(intent);
            }
        });
    }

    /**
     * AsyncTask
     */
    public class jsonClass extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... parms) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(parms[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader((stream)));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                return buffer.toString(); // return String of HTTP request

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            String newThreadName = "";

//          Log.d("Result = ",result);
            if (ChatListActivity.asyncTaskType.equals(ChatListActivity.GET_USER_LIST)) {
                 // check for async Task state for Get user list

                if (!result.equals("[]")) {
                    try {
                        JSONObject data = new JSONObject(result);
                        for (int i = 0; i < data.length(); i++) {
                            Log.d("DATA = ", data.getJSONObject("user_" + i).toString());
                            chatUserListData.add(data.getJSONObject("user_" + i).getString("username").toString());
                            chatUserListDataID.add(Integer.parseInt(data.getJSONObject("user_" + i).getString("id").toString()));
                        }

                        ListAdapter theAdapter = new ArrayAdapter<String>(getApplication(), android.R.layout.simple_list_item_1,
                                chatUserListData) {
                            @Override
                            public View getView(int position, View contentView, ViewGroup parent) {
                                View view = super.getView(position, contentView, parent);
                                TextView pro = (TextView) view.findViewById(android.R.id.text1);
                                pro.setTextColor(Color.BLACK);
                                return view;
                            }
                        };

                        chatUsersList.setAdapter(theAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplication(), "No Users Found", Toast.LENGTH_LONG).show();
                }

            } else if(ChatListActivity.asyncTaskType.equals(ChatListActivity.GET_CHAT_THREAD_LIST)){
                // check async task state for get chat thread list
                ArrayList<String> chatThreads = new ArrayList<>();
                String loggedUserName = new Login().getName();

                if (!result.equals("[]")) {
                    try {
                        JSONObject data = new JSONObject(result);
                        for (int i = 0; i < data.length(); i++) {
                            Log.d("CHAT Thread DATA = ", data.getJSONObject("chat_thread_" + i).toString());

                            chatThreads.add(data.getJSONObject("chat_thread_" + i).getString("chatThreadName").toString());

                            String threadName = data.getJSONObject("chat_thread_" + i).getString("chatThreadName").toString();
                            if(threadName.equals(ChatListActivity.selectedUserName+"_"+loggedUserName) || threadName.equals(loggedUserName+"_"+ChatListActivity.selectedUserName)){
                                // thread found
                                Log.d("Thread Found","");


                                Intent intent = new Intent(getApplication(), chatMainActivity.class);
                                intent.putExtra("userName", threadName);
                                intent.putExtra("userID", ChatListActivity.selectedUserID);

                                startActivity(intent);

                                break;
                            } else {
                                // no thread found
                                Log.d("No Thread Found","");

                                newThreadName = loggedUserName+"_"+ChatListActivity.selectedUserName;

                                ChatListActivity.asyncTaskType = ChatListActivity.INSERT_CHAT_THREAD; // set current state
                                new jsonClass().execute("http://" + IP.getMyIP() + "/Android/chatThreadNameList.php?insertNewThread="+newThreadName);

                                break;
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplication(), "No Users Found", Toast.LENGTH_LONG).show();

                    // no thread found
                    Log.d("No Thread Found","");

                    newThreadName = loggedUserName+"_"+ChatListActivity.selectedUserName;

                    ChatListActivity.asyncTaskType = ChatListActivity.INSERT_CHAT_THREAD; // set current state
                    new jsonClass().execute("http://" + IP.getMyIP() + "/Android/chatThreadNameList.php?insertNewThread="+newThreadName);
                }

            } else if(ChatListActivity.asyncTaskType.equals(ChatListActivity.INSERT_CHAT_THREAD)){
                // check async task state for insert chat thread
                Log.d("New Thread Found"," Created");


                Intent intent = new Intent(getApplication(), chatMainActivity.class);
                intent.putExtra("userName", newThreadName);
                intent.putExtra("userID", ChatListActivity.selectedUserID);

                startActivity(intent);
            }
        }
    }
}

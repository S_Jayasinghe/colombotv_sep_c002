package com.example.sjayasinghe.colombotvprograms;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class programList extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    ListView listview;
    String[] favoriteTVShows = {"Pushing Daisies", "Better Off Ted",
            "Twin Peaks", "Freaks and Geeks", "Orphan Black", "Walking Dead",
            "Breaking Bad", "The 400", "Alphas", "Life on Mars","Freaks and Geeks", "Orphan Black","Freaks and Geeks", "Orphan Black"};
    ArrayList<String> programListData = new ArrayList<>();
    ArrayList<Integer> programListDataID = new ArrayList<>();
    Connectivity IP = new Connectivity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Initialize();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Chat function will be triggered from here!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.program_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // This is reserved for Translator function.Please do not assign anything else for this.
             Intent intent = new Intent(getApplication(),PostFeed.class);
             Intent translatorIntent = new Intent(getApplication(),translator.class);
             startActivity(translatorIntent);

        } else if (id == R.id.nav_gallery) {
            Intent pushmainIntent = new Intent(getApplication(),pushmain.class);
            startActivity(pushmainIntent);

        } else if (id == R.id.nav_slideshow) {
            //Reserved for Schedule Function.
            Intent programTimesIntent = new Intent(getApplication(),ProgramTimes.class);
            programTimesIntent.putExtra("day","Monday");
            startActivity(programTimesIntent);
        } else if (id == R.id.nav_manage) {
            //Reserved for Push notification manager

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {
            //Reserved for Chat
            //Please do not put anything here.

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void Initialize(){
        listview = (ListView) findViewById(R.id.listView1);
        /**
         * Get Program List from DB
         */
        new jsonClass().execute("http://"+IP.getMyIP()+"/Android/databaseProgramList.php");

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),programListData.get(position).toString(),Toast.LENGTH_SHORT).show();
                Program p = new Program(programListData.get(position).toString());
                Intent intent = new Intent(getApplication(),PostFeed.class);
                intent.putExtra("programName",programListData.get(position).toString());
                intent.putExtra("programID",programListDataID.get(position).toString());
                startActivity(intent);
            }
        });

    }

    /**
     * AsyncTask
     */
    public class jsonClass extends AsyncTask<String,Void,String>{
        @Override
        protected String doInBackground(String... parms){
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try{
                URL url = new URL(parms[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader((stream)));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while((line = reader.readLine()) != null ){
                    buffer.append(line);
                }

                return buffer.toString(); // return String of HTTP request

            }catch (MalformedURLException e){
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            } finally {
                if(connection != null){
                    connection.disconnect();
                }
                try{
                    if(reader != null){
                        reader.close();
                    }
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            //System.out.println("RESUL = "+result);
            Log.d("Result = ",result);
            if(!result.equals("[]")){
                try{
                    JSONObject data = new JSONObject(result);
                    for(int i=0;i<data.length();i++){
                        //String programName = data.getJSONObject("");
                        Log.d("DATA = ",data.getJSONObject("program_"+i).toString());
                        programListData.add(data.getJSONObject("program_"+i).getString("progName").toString());
                        programListDataID.add(Integer.parseInt(data.getJSONObject("program_"+i).getString("progID").toString()));
                    }

                    ListAdapter theAdapter = new ArrayAdapter<String>(getApplication(), android.R.layout.simple_list_item_1,
                            programListData){
                        @Override
                        public View getView(int position, View contentView, ViewGroup parent){
                            View view  = super.getView(position,contentView,parent);
                            TextView pro = (TextView) view.findViewById(android.R.id.text1);
                            pro.setTextColor(Color.BLACK);
                            return view;
                        }
                    };

                    listview.setAdapter(theAdapter);
                }catch(JSONException e){
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(getApplication(),"No Programs Found",Toast.LENGTH_LONG).show();
            }
        }
    }
}

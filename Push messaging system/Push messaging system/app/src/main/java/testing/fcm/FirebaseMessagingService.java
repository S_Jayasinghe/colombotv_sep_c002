package testing.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.RemoteMessage;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService{
    private usersRecord user = new usersRecord();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        /* 6 for personal chats, 5 for group chats*/

        String realMessage=remoteMessage.getData().get("message"); //The Message which has been received.
        boolean isNumeral=false;

        String selectionName="Group";
        String dupli=realMessage;
        boolean number=Character.isDigit(dupli.charAt(0));
        char first = dupli.charAt(0);
        int selection = Character.getNumericValue(first);
        //if(realMessage.matches("\\d+(?:\\.\\d+)?"))
        if(selection==5)
        {
            isNumeral=true; //Numeral has been sent, therefore a chat notification
        }
        else if(selection==6){
            isNumeral=true;
        }
        else
        {
            isNumeral=false;
        }

        if(isNumeral==false) {
            sendMessageToActivity(realMessage);

            showNotification(remoteMessage.getData().get("message"));

            String ding = "Push Messages Available";

            ShowToastInIntentService(ding);
        }
        else if(isNumeral==true){
            showMessage(realMessage);
            String testMsg = realMessage;
            String[] tokens = testMsg.split("#");


            if(selection==6){
                String indi = realMessage;
                String[] indiTok = indi.split("_");
                String userCurrent=user.getUser();

                if(userCurrent==indiTok[1]){
                   /* String messageChat= "Chat Message from "+indiTok[0];
                    showMessage(messageChat);*/
                }

            }else {
                String messageChat = "GroupChat Message from " + tokens[1];
                showMessage(messageChat);
            }
        }

        //  Toast.makeText(getApplication(),"This is Testing",Toast.LENGTH_LONG).show();
    }
    private void ShowToastInIntentService(final String sText)
    {  final Context MyContext = this;
        new Handler(Looper.getMainLooper()).post(new Runnable()
        {  @Override public void run()
        {  Toast toast1 = Toast.makeText(MyContext, sText, Toast.LENGTH_LONG);
            toast1.show();



        }
        });
    };

    private void sendMessageToActivity(String msg) {
        Intent intent = new Intent("intentKey");

        intent.putExtra("key", msg);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }



    private void showNotification(String message) {

        Intent i = new Intent(this,landpush.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("message",message);

       /* Intent intent = new Intent(getApplication(),PostFeed.class);
        intent.putExtra("programName",programListData.get(position).toString());
        intent.putExtra("programID",programListDataID.get(position).toString());*/

        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle("Colombo TV")
                .setContentText(message)
                .setSmallIcon(R.drawable.bell)

                .setLargeIcon(largeIcon)
                .setContentIntent(pendingIntent);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(0,builder.build());
    }

    private void showMessage(String message) {

        Intent i = new Intent(this,landpush.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("message",message);

       /* Intent intent = new Intent(getApplication(),PostFeed.class);
        intent.putExtra("programName",programListData.get(position).toString());
        intent.putExtra("programID",programListDataID.get(position).toString());*/

        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle("Colombo TV")
                .setContentText(message)
                .setSmallIcon(R.drawable.bell)

                .setLargeIcon(largeIcon)
                .setContentIntent(pendingIntent);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(0,builder.build());
    }


    public static String sendMessage(RemoteMessage remoteMessage) {
        String messageShown=remoteMessage.getData().get("message");
        // Toast.makeText(getApplication(),messageShown,Toast.LENGTH_LONG).show();
        return messageShown;
    }
}

package testing.fcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.*;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import java.lang.reflect.Type;

public class landpush extends AppCompatActivity {
    private static String globalString;
    ListView listpush;
    private static boolean state=true;
    private usersRecord user = new usersRecord();


   // private ArrayList<ProgramData> programList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String current=user.getUser();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landpush);

        Intent intent = getIntent();
        globalString = intent.getStringExtra("message");

        try {
            if (globalString.isEmpty()) {
                globalString = "Push Notifications from ColomboTV";
            }
        }catch(Exception e){
                globalString = "Push Notifications from ColomboTV";
                //state=false;
        }

        TextView text = (TextView) findViewById(R.id.textView);
        text.setText(globalString);

        this.initialize("empty");
        LocalBroadcastManager.getInstance(getApplication()).registerReceiver(
                mMessageReceiver, new IntentFilter("intentKey"));

        FirebaseMessaging.getInstance().subscribeToTopic("test");
        FirebaseInstanceId.getInstance().getToken();

    }

    private void initialize(String value){



        /*Store to the shared preference
        Get from the shared preference and then append*/

        String valueThis=value;

        listpush = (ListView) findViewById(R.id.listViewPush);
        List<String> pushList = new ArrayList<String>();
        /*pushList.add("foo");
        pushList.add("bar");
        pushList.add("bar");pushList.add("bar");pushList.add("bar");pushList.add("bar");pushList.add("bar");pushList.add("bar");pushList.add("bar");*/


        SharedPreferences prefs;
        SharedPreferences.Editor editor;
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = prefs.edit();
        //JSONArray jArray = new JSONArray(pushList);

        if(value!="empty") {
            //User has saved values.
            //Retrieve the previously saved shared-preference.
            String key="";
            ArrayList<String> arrayBorn = new ArrayList<String>();
            String jArrayString = prefs.getString(key, "NOPREFSAVED");
            if (jArrayString.matches("NOPREFSAVED"))
            {
                //Toast
            }
            else {
                try {
                    JSONArray jArray = new JSONArray(arrayBorn);
                    jArray = new JSONArray(jArrayString);
                    for (int i = 0; i < jArray.length(); i++) {
                        arrayBorn.add(jArray.getString(i));
                    }

                } catch (JSONException e) {

                }

            }

            //Push new values to the arrayBorn.
            arrayBorn.add(value);
            JSONArray jArray = new JSONArray(arrayBorn);
            editor.remove(key);
            editor.putString(key, jArray.toString());
            editor.commit();

            /*Reverse the array to be shown*/
            Collections.reverse(arrayBorn);
           /* ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                    this,
                    android.R.layout.simple_list_item_1,
                    arrayBorn );*/

            ListAdapter theAdapter = new ArrayAdapter<String>(getApplication(), android.R.layout.simple_list_item_1,
                    arrayBorn){
                @Override
                public View getView(int position, View contentView, ViewGroup parent){
                    View view  = super.getView(position,contentView,parent);
                    TextView pro = (TextView) view.findViewById(android.R.id.text1);
                    pro.setTextColor(Color.BLACK);
                    return view;
                }
            };
            listpush.setAdapter(theAdapter);


        }
        else{
            //User has not saved any values.
            String key="";
            ArrayList<String> arrayBorn = new ArrayList<String>();
            JSONArray jArray = new JSONArray(arrayBorn);

            String jArrayString = prefs.getString(key, "NOPREFSAVED");
            if (jArrayString.matches("NOPREFSAVED"))
            {
                //Toast
            }
            else {
                try {
                    jArray = new JSONArray(jArrayString);
                    for (int i = 0; i < jArray.length(); i++) {
                        arrayBorn.add(jArray.getString(i));
                    }

                } catch (JSONException e) {

                }

                /*Reverse the array to be shown*/
                Collections.reverse(arrayBorn);
                /*ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                        this,
                        android.R.layout.simple_list_item_1,
                        arrayBorn );

                listpush.setAdapter(arrayAdapter);*/
                ListAdapter theAdapter = new ArrayAdapter<String>(getApplication(), android.R.layout.simple_list_item_1,
                        arrayBorn){
                    @Override
                    public View getView(int position, View contentView, ViewGroup parent){
                        View view  = super.getView(position,contentView,parent);
                        TextView pro = (TextView) view.findViewById(android.R.id.text1);
                        pro.setTextColor(Color.BLACK);
                        return view;
                    }
                };
                listpush.setAdapter(theAdapter);
            }
        }



        /*Code used to retrive values from the
        sharedpreferences*/










    }
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("key");
            TextView text = (TextView) findViewById(R.id.textView);
            text.setText(message);
            globalString=message;
            // Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

        }
    };


    public void savePush(View view) {

                if((globalString==null)||(globalString=="Push Notifications from ColomboTV")){
                    Toast.makeText(getApplication(), "New Broadcasts are Not Found!", Toast.LENGTH_LONG).show();
                }
                else {
                    String value = "Test";
                    this.initialize(globalString);
                    globalString = null;
                }

    }

    public void populateList(View view){

        //listpush
    }
}
